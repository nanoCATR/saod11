﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Stack2
    {
        private int n;
        private int[] s;
        public Stack2()
        {
            s = new int[0];
            n = 0;
        }

        public void push(int x)
        {
            System.Array.Resize(ref s, n + 1);
            s[n] = x;
            n++;
        }

        public int top()
        {
            if (n != 0)
            {
                return s[n - 1];
            }
            return -1;
        }
        void pop()
        {
            if (n != 0)
            {
                System.Array.Resize(ref s, n - 1);
                n--;
            }
        }
        public bool empty()
        {
            if (n == 0)
                return true;
            else return false;
        }
        public int size()
        {
            return n;
        }
    }
}
