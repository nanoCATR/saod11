﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Stack1
    {

        private int n;
        private int z;
        private int[] s;

        public Stack1(int k)
        {
            s = new int[k];
            n = 0;
            z = k;
        }

        public void push(int x)
        {
            if (n == z) return;
            else
            {
                s[n] = x;
                n++;
            }
        }
        public int top()
        {
            if (n != 0)
            {
                return s[n - 1];
            }
            return -1;
        }
        void pop()
        {
            if (n != 0)
                n--;
        }
        public bool empty()
        {
            if (n == 0)
                return true;
            else return false;
        }
        public int size()
        {
            return n;
        }
    }
}
